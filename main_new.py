"""
Push modules
============

$ mpremote cp main.py :


Interactive test with mpremote
==============================

$ mpremote
>>> 
"""

from configsystem import SLEEP_TIME_SEC, STOP_ON_EXCEPTION

import time

from sntime import get_current_datetime_str
from snlogs import write_log
from snesp import step


# MAIN LOOP ###################################################################

def main():

    while True:
        try:
            step()

        except Exception as e:
            msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred while reading sensor values in main()\n\n"
            write_log(msg_str, print_msg=True)
            if STOP_ON_EXCEPTION:
                raise

        # WAIT
        print(f"Sleep {SLEEP_TIME_SEC} seconds")
        time.sleep(SLEEP_TIME_SEC)

if __name__ == '__main__':
    main()
