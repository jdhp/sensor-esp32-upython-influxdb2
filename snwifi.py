import network
import time

from configwifi import WLAN_SSID, WLAN_PASS

def connect_to_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        wlan.connect(WLAN_SSID, WLAN_PASS)
        while not wlan.isconnected():
            # https://forum.micropython.org/viewtopic.php?t=2440#p14321
            print(".", end="")
            time.sleep(10)
    print(wlan.ifconfig())
    #time.sleep(10)

def disconnect_from_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.disconnect()
    wlan.active(False)
