# TODO:

- [ ] Log line error in case of exception : https://forum.pycom.io/topic/4409/exceptions-in-micro-python
        try:
            1/0
        except Exception as e:
            import sys
            with open("error.log", "a") as f:
                sys.print_exception(e, f)
- [ ] Implement a D3.js time series viewer (that e.g. fetch data from InfluxDB cloud) so that data can be seen everywhere and without InfluxDB account + add a DockerFile to setup/run a webserver
- [ ] Add sensors (air quality, air pressure, door/window opening, motion detector)
- [ ] Make an actual PCB (1st version) -> e.g. https://www.framboise314.fr/fabrication-de-circuits-imprimes-pcb-chez-jlcpcb/
- [ ] Work on battery (+ monitor battery level)
- [ ] Add low energy AVR MCU to measure continuous data: light, sound, accelerometer, ... and to send data only when a given event is triggered (alarm, ...)
- [ ] Make an actual PCB (2nd version)
- [ ] Deepsleep (no while loop anymore, main.py -> boot.py, ...) and wifi disconnect as in https://www.hackster.io/nacktnasenwombat/temperature-humidity-measurement-with-nodemcu-191c46
- [ ] Bluetooth connection
- [ ] Implement a measurement buffer: in case there is a connection issue, accumulate measurements in memory and upload them when the connection issue is fixed (can also be used to reduce the WIFI usage e.g. if ESP works on batteries)
- [ ] Design and print a case (similar to the Nest Smoke Detector)
