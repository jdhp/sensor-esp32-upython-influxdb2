"""
C.f. https://docs.micropython.org/en/v1.15/esp8266/tutorial/onewire.html#controlling-1-wire-devices

C.f. https://lastminuteengineers.com/multiple-ds18b20-arduino-tutorial/ to interface several DS18B20 to the same PIN
(that's why the "scan" method is called in the "measure" method)
"""

import ds18x20
import machine
import onewire
import ubinascii
import time

from sntime import get_current_timestamp, get_current_datetime_str

class SNDS18X20:

    def __init__(self, gpio_pin, sensor_location, sensor_version="DS18B20"):
        self.gpio_pin = gpio_pin
        self.sensor_location = sensor_location
        self.sensor_version = sensor_version

        assert self.sensor_version in ("DS18S20", "DS18B20")

        # Create the onewire object
        self.ds = ds18x20.DS18X20(onewire.OneWire(machine.Pin(self.gpio_pin)))


    def measure(self):
        current_timestamp = get_current_timestamp()
        #current_datetime_str = get_current_datetime_str()

        # Scan for devices on the bus
        roms = self.ds.scan()
        #print('Onewire devices:', roms)

        # Note that you must execute the convert_temp() function to initiate a temperature reading,
        # then wait at least 750ms before reading the value.
        # C.f. https://docs.micropython.org/en/v1.15/esp8266/tutorial/onewire.html#controlling-1-wire-devices
        self.ds.convert_temp()
        time.sleep_ms(750)

        for rom in roms:  # FYI rom is just a byte array
            temperature = self.ds.read_temp(rom)

            sensor_id = ubinascii.hexlify(rom).decode('utf-8')

            measure_dict_list = [
                {
                    "current_timestamp": current_timestamp,
                    "measurement": "temperature",
                    "value": temperature,
                    "sensor_name": self.sensor_version,
                    "sensor_id": sensor_id,  #self.sensor_id,
                    "sensor_location": self.sensor_location,
                    "unit": "Celsius"
                }
            ]

        return measure_dict_list
