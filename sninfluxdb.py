import urequests

from configsystem import SYSTEM_ID, STOP_ON_EXCEPTION
from configbackend import POST_URL, HEADER_DICT

from sntime import get_current_datetime_str
from snlogs import write_log


INFLUXDB_WRITE_SUCCESS_CODE = 204   # https://help.influxcloud.net/tips_and_faqs/

def post_measurement(current_timestamp, measurement, value, sensor_id, sensor_location, unit):
    try:
        if isinstance(value, dict):
            value_item_str = ",".join(["{}={}".format(k, v) for k,v in value.items()])
        else:
            value_item_str = "value={}".format(value)

        data_str = f'{measurement},system_id={SYSTEM_ID},sensor_id={sensor_id},sensor_location={sensor_location},unit={unit} {value_item_str} {current_timestamp}'
        print(data_str)
        resp = urequests.post(POST_URL, data=data_str, headers=HEADER_DICT)

        if resp.status_code != INFLUXDB_WRITE_SUCCESS_CODE:
            msg_str = f"{get_current_datetime_str()}: {str(e)}; "
            msg_str += "Error while sending measurement in post_measurement(); "
            msg_str += f"Returned InfluxDB status_code is {str(resp.status_code)}; "
            msg_str += f"Returned InfluxDB reason is {resp.reason.decode('utf-8')};"
            msg_str += f"The raw response is {resp.text}\n\n"
            write_log(msg_str, print_msg=True)

        resp.close()   # c.f. https://forum.micropython.org/viewtopic.php?t=7771
    except Exception as e:
        msg_str = f"{get_current_datetime_str()}: {str(e)}; An unknown error occurred while sending measurement in post_measurement();"
        write_log(msg_str, print_msg=True)
        if STOP_ON_EXCEPTION:
            raise