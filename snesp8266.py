import machine

def deepsleep(sleep_time_sec):
    # https://docs.micropython.org/en/latest/esp8266/tutorial/powerctrl.html#deep-sleep-mode
    # Configure RTC.ALARM0 to be able to wake the device
    rtc = machine.RTC()
    rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)

    # Set RTC.ALARM0 to fire after SLEEP_TIME_SEC seconds (waking the device)
    rtc.alarm(rtc.ALARM0, sleep_time_sec * 1000)

    # Put the device to sleep
    machine.deepsleep()