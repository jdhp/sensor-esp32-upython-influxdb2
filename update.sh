#!/bin/bash

# The MIT License
# 
# Copyright (c) 2020 Jérémie DECOCK <jd.jdhp@gmail.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# TODO:
# - vérif si le fichier "ESP_UNIQUE_ID" existe sur l'ESP
# - s'il n'existe pas, renommer name.py en name.old.py sur l'ESP, push "print_esp_unique_id.py" en "main.py" sur l'ESP et demander à l'utilisateur de 1) redémarrer l'ESP puis relancer ce script (update.sh)
# - si l'ESP_UNIQUE_ID n'a pas de config dans esp_config_files : 1) créer un répertoire dans esp_config_files et copier le "sensorconfig.py" de l'ESP dedans ; 2) demaner à l'utilisateur de compléter/maj le fichier sensorconfig.py ; 4) redémarrer ce script

MSG_REPLUG="Please disconnect and reconnect your ESP to your computer's USB port"


# COMMON CONFIG ###############################################################

#export ESP32_FIRMWARE="firmwares/esp32-20220117-v1.18.bin"
export ESP32_FIRMWARE="firmwares/esp32-20220618-v1.19.1.bin"

CURRENT_PATH=$(pwd)


# INTRO #######################################################################

whiptail --msgbox "Please unplug all devices from your ESP then press OK to continue." 8 78

whiptail --msgbox "Please plug your ESP to this computer then press OK to continue." 8 78


# UPDATE THE FIRMWARE #########################################################

if (whiptail --title "Update the MicroPython Firmware?" --yesno "Do you want to update the µpython firmware?" 8 78) then

    # C.f. http://www.micropython.org/download/esp32/
    # C.f. http://www.micropython.org/download/esp8266/ for the ESP8266-12F (4 Mo Flash)
    echo
    echo "Erase flash"
    echo "==========="
    #esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
    esptool.py --chip auto --port /dev/ttyUSB0 erase_flash

    echo
    echo "Write firmware"
    echo "=============="
    echo "Writing ${ESP32_FIRMWARE}"

    # ESP32   -> C.f. http://www.micropython.org/download/esp32/
    # ESP8266 -> C.f. https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html#deploying-the-firmware

    ESP_CHIP=$(whiptail --title "ESP Chip" --radiolist \
                            "Choose ESP chip" 40 80 4 \
                            "ESP32-WROOM32" "ESP32-WROOM 32" ON \
                            "ESP8266-12F" "ESP8266-12F" OFF 3>&1 1>&2 2>&3)
    case "${ESP_CHIP}" in
        ("ESP32-WROOM32") esptool.py --after hard_reset --chip auto --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 ${ESP32_FIRMWARE} ;;
        ("ESP8266-12F")   esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect -fm dout 0 ${ESP32_FIRMWARE} ;;
        (*) echo "Error" ;;
    esac

    ##esptool.py --after hard_reset --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 ${ESP32_FIRMWARE}
    #esptool.py --after hard_reset --chip auto --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 ${ESP32_FIRMWARE}
    #esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect -fm dout 0 ${ESP32_FIRMWARE}

    #whiptail --msgbox "${MSG_REPLUG}" 8 78

    sleep 5  # Wait for the end of the hard reset
fi


# GET THE ESP UNIQUE ID #######################################################

#export ESP32_UNIQUE_ID=$(ampy -p /dev/ttyUSB0 -b 115200 run 'snsystemid.py' | tr -d '\r')   # ACTUALLY, ESP32 UNIQUE ID IS ITS MAC ADDRESS!
export ESP32_UNIQUE_ID=$(mpremote run snsystemid.py | tr -d '\r')   # ACTUALLY, ESP32 UNIQUE ID IS ITS MAC ADDRESS!
#export ESP32_MAC_ADDR=$(esptool.py --chip esp32 --port /dev/ttyUSB0 read_mac | grep "^MAC:" | head -n 1 | tr -d " :" | cut -c4- )    # THIS RETURNS EXACTLY THE SAME ID THAN THE LINE JUST ABOVE...
echo "ESP32 UNIQUE ID: ${ESP32_UNIQUE_ID}"

export ESP32_CONFIG_DIR="esp_config_files/${ESP32_UNIQUE_ID}"


# IS THERE A CONFIG FILE FOR THIS ESP UNIQUE ID? ##############################

if [ ! -d "${ESP32_CONFIG_DIR}" ] 
then
    echo "Directory ${ESP32_CONFIG_DIR} does not exist." 
    mkdir "${ESP32_CONFIG_DIR}"
fi

if [ ! -f "${ESP32_CONFIG_DIR}/configwifi.py" ] 
then
    WIFI_AP_CHOICE=$(whiptail --title "Wifi" --radiolist \
                            "Choose wifi AP" 40 80 4 \
                            "WORK" "Work AP" ON \
                            "HOME" "Home AP" OFF 3>&1 1>&2 2>&3)
    case "${WIFI_AP_CHOICE}" in
        ("HOME") cp private/wifi/home.py "${ESP32_CONFIG_DIR}/configwifi.py" ;;
        ("WORK") cp private/wifi/work.py "${ESP32_CONFIG_DIR}/configwifi.py" ;;
        (*) echo "Error" ;;
    esac
fi

if [ ! -f "${ESP32_CONFIG_DIR}/configbackend.py" ] 
then
    BACKEND_CHOICE=$(whiptail --title "Backend" --radiolist \
                            "Choose database backend" 40 80 4 \
                            "INFLUXDB_CLOUD_PERSO" "InfluxDB Cloud perso" ON \
                            "INFLUXDB_LOCALHOST" "InfluxDB localhost" OFF 3>&1 1>&2 2>&3)
    case "${BACKEND_CHOICE}" in
        ("INFLUXDB_CLOUD_PERSO") cp private/backend/influxdb_cloud_perso.py "${ESP32_CONFIG_DIR}/configbackend.py" ;;
        ("INFLUXDB_LOCALHOST")   cp private/backend/influxdb_localhost.py   "${ESP32_CONFIG_DIR}/configbackend.py" ;;
        (*) echo "Error" ;;
    esac
fi

if [ ! -f "${ESP32_CONFIG_DIR}/configsystem.py" ] 
then
    echo "${ESP32_CONFIG_DIR}/configsystem.py does not exist." 
    cp templates/configsystem.py.template "${ESP32_CONFIG_DIR}/configsystem.py"
    whiptail --msgbox "Please edit ${ESP32_CONFIG_DIR}/configsystem.py then press OK to continue." 8 78
fi

if [ ! -f "${ESP32_CONFIG_DIR}/configsensors.py" ] 
then
    echo "${ESP32_CONFIG_DIR}/configsensors.py does not exist." 
    cp templates/configsensors.py.template "${ESP32_CONFIG_DIR}/configsensors.py"
    whiptail --msgbox "Please edit ${ESP32_CONFIG_DIR}/configsensors.py then press OK to continue." 8 78
fi


# IS THERE A CONFIG FILE FOR THIS ESP UNIQUE ID? ##############################

echo
echo "Loading config for ESP #${ESP32_UNIQUE_ID}..."
echo

cd ${ESP32_CONFIG_DIR}
#ampy -p /dev/ttyUSB0 -b 115200 put 'configsensors.py'
#ampy -p /dev/ttyUSB0 -b 115200 put 'configwifi.py'
#ampy -p /dev/ttyUSB0 -b 115200 put 'configbackend.py'
mpremote cp conf*.py :

cd ../..

mpremote cp sn*.py :

if (whiptail --title "Deep Sleep" --yesno "Do you want to use the Deep Sleep implementation (boot.py) ? The PMS5003 particle sensor won't work in the deep sleep implementation." 8 78) then
    echo "Copy boot.py"
    #ampy -p /dev/ttyUSB0 -b 115200 put 'boot.py'
    mpremote cp boot.py :
else
    echo "Copy main.py"
    #ampy -p /dev/ttyUSB0 -b 115200 put 'main.py'
    #ampy -p /dev/ttyUSB0 -b 115200 put 'pms5003.py'
    mpremote cp main.py :
    mpremote cp pms5003.py :

    #echo
    #echo "Written config"
    #echo "=============="
    #ampy -p /dev/ttyUSB0 -b 115200 get configsensors.py
    #whiptail --title "Written config" --textbox /dev/stdin 60 200 <<< "$(ampy -p /dev/ttyUSB0 -b 115200 get configsensors.py | tr -d '\r')"
    #whiptail --title "Written config" --textbox /dev/stdin 60 200 <<< "$(mpremote cat :configsensors.py | tr -d '\r')"
    whiptail --title "Written config" --textbox /dev/stdin 60 200 <<< "$(mpremote ls | tr -d '\r')"

    #whiptail --msgbox "${MSG_REPLUG} and check that everything is working properly with the following command: picocom --baud=115200 /dev/ttyUSB0" 8 78

    if (whiptail --yesno "Do you want to reset and run picocom?" 8 78) then
        #esptool.py --after hard_reset --chip esp32 --port /dev/ttyUSB0 --baud 460800 read_mac
        mpremote reset

        #picocom --baud=115200 /dev/ttyUSB0
        mpremote repl
    fi
fi