"""
Push modules
============

$ mpremote cp main.py :


Interactive test with mpremote
==============================

$ mpremote
>>> 
"""

from configsystem import SYSTEM_CHIP, SLEEP_TIME_SEC, STOP_ON_EXCEPTION

import time

from sntime import get_current_datetime_str
from snlogs import write_log
from snesp import step

if SYSTEM_CHIP == "ESP32":
    from snesp32 import deepsleep
elif SYSTEM_CHIP == "ESP8266":
    from snesp8266 import deepsleep
else:
    write_log(f"Unknown chip {SYSTEM_CHIP}", print_msg=True)


# MAIN LOOP ###################################################################

def main():

    try:
        step()

    except Exception as e:
        msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred while reading sensor values in main()\n\n"
        write_log(msg_str, print_msg=True)
        if STOP_ON_EXCEPTION:
            raise

    # WAIT
    deepsleep(SLEEP_TIME_SEC)


if __name__ == '__main__':
    main()
