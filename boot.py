"""
Push modules
============

$ ampy -p /dev/ttyUSB0 -b 115200 put 'main.py'


Interactive test with picocom
=============================

$ picocom --baud=115200 /dev/ttyUSB0

>>> import main
>>> dht_obj, ldr_adc_obj = main.init()
>>> dht_obj.measure()
>>> dht_obj.temperature()
21.8
>>> dht_obj.humidity()
42.8
"""

from configbackend import BACKEND_TYPE
from configsensors import *
from configsystem import SYSTEM_ID, SYSTEM_LOCATION, SLEEP_TIME_SEC, STOP_ON_EXCEPTION

import dht
import esp
import esp32
import machine
import ujson
import time

from snwifi import connect_to_wifi, disconnect_from_wifi
from sntime import synchronize_ntp, get_current_timestamp, get_current_datetime_str
from snlogs import write_log

if BACKEND_TYPE == "RESTFUL":
    from sninfluxdb import post_measurement
elif BACKEND_TYPE == "INFLUXDB2":
    from snrest import post_measurement
else:
    write_log(f"Unknown backend {BACKEND_TYPE}", print_msg=True)


# INIT ########################################################################

def init():

    # SENSOR OBJECTS ##########################################################

    if DHT22_SENSOR_CONFIG is not None:
        for dht_dict in DHT22_SENSOR_CONFIG:
            dht_obj = dht.DHT22(machine.Pin(dht_dict["pin"]))
            dht_dict["dht_obj"] = dht_obj

    if LDR_SENSOR_CONFIG is not None:
        for ldr_dict in LDR_SENSOR_CONFIG:
            ldr_dict["adc_obj"] = machine.ADC(machine.Pin(ldr_dict["pin"]))

    if HCSR501_PIR_SENSOR_CONFIG is not None:
        for pir_sensor_dict in HCSR501_PIR_SENSOR_CONFIG:
            pir_sensor_dict["pin"].irq(trigger=machine.Pin.IRQ_RISING, handler=hcsr501_pir_callback)  # TODO: Is it ok if pir_pin object is destroyed at the end for this function ???
            pir_sensor_dict["count"] = 0


# SENSORS #####################################################################

def hcsr501_pir_callback(pin):
    for pir_sensor_dict in HCSR501_PIR_SENSOR_CONFIG:
        if pir_sensor_dict["pin"] == pin:
            pir_sensor_dict["count"] += 1
            break

    #print(pin, time.time(), pin.value())


def read_and_post_hcsr501_pir_sensors():
    if HCSR501_PIR_SENSOR_CONFIG is not None:
        for pir_sensor_dict in HCSR501_PIR_SENSOR_CONFIG:
            current_timestamp = get_current_timestamp()

            post_measurement(current_timestamp=current_timestamp,
                             measurement="PIR",
                             value=pir_sensor_dict["count"],
                             sensor_id=pir_sensor_dict["sensor_id"],
                             sensor_location=HCSR501_PIR_SENSOR_LOCATION,
                             unit="Triggers")
        
            pir_sensor_dict["count"] = 0


def read_and_post_dht22_sensors():
    if DHT22_SENSOR_CONFIG is not None:
        for dht_dict in DHT22_SENSOR_CONFIG:
            current_timestamp = get_current_timestamp()

            dht_dict["dht_obj"].measure()

            post_measurement(current_timestamp=current_timestamp,
                             measurement="temperature",
                             value=dht_dict["dht_obj"].temperature(),
                             sensor_id=dht_dict["sensor_id"],
                             sensor_location=DHT22_SENSOR_LOCATION,
                             unit="Celsius")

            post_measurement(current_timestamp=current_timestamp,
                             measurement="humidity",
                             value=dht_dict["dht_obj"].humidity(),
                             sensor_id=dht_dict["sensor_id"],
                             sensor_location=DHT22_SENSOR_LOCATION,
                             unit="%")


def read_and_post_ldr_sensor():
    if LDR_SENSOR_CONFIG is not None:
        for ldr_dict in LDR_SENSOR_CONFIG:
            current_timestamp = get_current_timestamp()

            ldr_raw_level = ldr_dict["adc_obj"].read()
            ldr_percent_level = float(ldr_raw_level) / 4095. * 100.

            post_measurement(current_timestamp=current_timestamp,
                            measurement="light",
                            value=ldr_percent_level,
                            sensor_id=ldr_dict["sensor_id"],
                            sensor_location=LDR_SENSOR_LOCATION,
                            unit="%")


def read_and_post_esp32_internal_sensors():
    current_timestamp = get_current_timestamp()

    # http://docs.micropython.org/en/latest/esp32/quickref.html#general-board-control
    esp_flash_size = esp.flash_size()
    esp32_hall_sensor = esp32.hall_sensor()                   # read the internal hall sensor
    esp32_mcu_temperature_farenheit = esp32.raw_temperature() # read the internal temperature of the MCU, in Farenheit
    esp32_mcu_temperature_celsius = (esp32_mcu_temperature_farenheit - 32.0) / 1.8

    post_measurement(current_timestamp=current_timestamp,
                     measurement="flash_size",
                     value=esp_flash_size,
                     sensor_id=SYSTEM_ID,
                     sensor_location=SYSTEM_LOCATION,
                     unit="raw")

    post_measurement(current_timestamp=current_timestamp,
                     measurement="hall_sensor",
                     value=esp32_hall_sensor,
                     sensor_id=SYSTEM_ID,
                     sensor_location=SYSTEM_LOCATION,
                     unit="raw")

    post_measurement(current_timestamp=current_timestamp,
                     measurement="esp32_internal_temperature",
                     value=esp32_mcu_temperature_celsius,
                     sensor_id=SYSTEM_ID,
                     sensor_location=SYSTEM_LOCATION,
                     unit="Celsius")



# MAIN LOOP ###################################################################

def main():

    try:
        init()

        connect_to_wifi()
        synchronize_ntp()

        read_and_post_dht22_sensors()                # POST TEMPERATURE AND HUMIDITY (DHT22)
        read_and_post_ldr_sensor()                   # POST LIGHT LEVEL (LDR)
        read_and_post_esp32_internal_sensors()       # ESP32 INTERNAL SENSORS
        read_and_post_hcsr501_pir_sensors()

        disconnect_from_wifi()

    except Exception as e:
        msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred in main()\n\n"
        write_log(msg_str, print_msg=True)
        if STOP_ON_EXCEPTION:
            raise

    # WAIT
    machine.deepsleep(SLEEP_TIME_SEC * 1000)  # (Deep) sleep for 3 seconds ; c.f. https://forum.micropython.org/viewtopic.php?t=5371#p30958


if __name__ == '__main__':
    main()
