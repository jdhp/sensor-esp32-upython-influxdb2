from configbackend import BACKEND_TYPE
from configsensors import SENSOR_LIST
from configsystem import SYSTEM_ID, SYSTEM_LOCATION, STOP_ON_EXCEPTION

from snwifi import connect_to_wifi, disconnect_from_wifi
from sntime import synchronize_ntp, get_current_timestamp, get_current_datetime_str
from snlogs import write_log

if BACKEND_TYPE == "RESTFUL":
    from snrest import post_measurement
elif BACKEND_TYPE == "INFLUXDB2":
    from sninfluxdb import post_measurement
else:
    write_log(f"Unknown backend {BACKEND_TYPE}", print_msg=True)


def step():
    connect_to_wifi()
    synchronize_ntp()

    current_timestamp = get_current_timestamp()

    for sensor in SENSOR_LIST:
        try:
            measure_dict_list = sensor.measure()
            for measure_dict in measure_dict_list:
                post_measurement(current_timestamp=current_timestamp, #measure_dict["current_timestamp"],
                                 measurement=measure_dict["measurement"],
                                 value=measure_dict["value"],
                                 sensor_id=measure_dict["sensor_id"],
                                 sensor_location=measure_dict["sensor_location"],
                                 unit=measure_dict["unit"])

        except Exception as e:
            msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred while reading sensor values in main()\n\n"
            write_log(msg_str, print_msg=True)
            if STOP_ON_EXCEPTION:
                raise

    disconnect_from_wifi()