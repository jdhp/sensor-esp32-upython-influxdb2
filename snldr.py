import machine

from sntime import get_current_timestamp, get_current_datetime_str


class SNLDR:

    def __init__(self, adc_pin, sensor_id, sensor_location, esp_version="ESP32"):
        self.adc_pin = adc_pin
        self.sensor_id = sensor_id
        self.sensor_location = sensor_location
        self.esp_version = esp_version

        if self.esp_version == "ESP32":
            self.adc = machine.ADC(machine.Pin(self.adc_pin))  # GPIO Pin (TODO ?)
        elif self.esp_version == "ESP8266":
            self.adc = machine.ADC(self.adc_pin)
        else:
            raise ValueError(f"Unknown ESP version {self.version}")


    def measure(self):
        current_timestamp = get_current_timestamp()
        #current_datetime_str = get_current_datetime_str()

        ldr_raw_value = self.adc.read()

        if self.esp_version == "ESP32":
            adc_max_value = 4095.  # 12 bits resolution
        elif self.esp_version == "ESP8266":
            adc_max_value = 1024.  # 10 bits resolution
        else:
            raise ValueError(f"Unknown ESP version {self.version}")

        ldr_percent_level = float(ldr_raw_value) / adc_max_value * 100.

        measure_dict_list = [
            {
                "current_timestamp": current_timestamp,
                "measurement": "light",
                "value": ldr_percent_level,
                "sensor_name": "LDR",
                "sensor_id": self.sensor_id,
                "sensor_location": self.sensor_location,
                "unit": "%"
            }
        ]

        return measure_dict_list
