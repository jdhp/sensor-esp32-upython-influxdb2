# C.f. https://bhave.sh/micropython-mqtt/ (section 6)

import umqtt
#import umqtt.simple
#import umqtt.robust    # TODO
from umqtt.simple import MQTTClient

from configsystem import SYSTEM_ID, STOP_ON_EXCEPTION
from configbackend import MQTT_BROKER_IP, MQTT_TOPIC_PREFIX

from sntime import get_current_datetime_str
from snlogs import write_log


def post_measurement(mqtt_payload_dict, sensor_name, sensor_id, sensor_location):
    try:
        mqttc = MQTTClient("esp32" + sensor_id, MQTT_BROKER_IP, keepalive=60)
        mqttc.connect()

        mqtt_topic = f"{MQTT_TOPIC_PREFIX}/{sensor_location}/{sensor_name}/{sensor_id}"
        # mqtt_payload_str = str(mqtt_payload_dict)  # BUG: json.decoder.JSONDecodeError: Expecting property name enclosed in double quotes: line 1 column 2 (char 1) lines 432507-432573/432573 (END)
        mqtt_payload_str = "{" + ",".join([f'"{k}":{v}' for k, v in mqtt_payload_dict.items()]) + "}"   # This seems ugly but the much simpler "str(mqtt_payload_dict)" statement doesn't work well... it causes a bug in json.load() which expects double quotes on dict keys but "str()" use simple quotes... => json.load() fails in mqtt2influx daemon...

        print(f"Publish to MQTT broker {MQTT_BROKER_IP} topic {mqtt_topic} payload {mqtt_payload_str}")
        mqttc.publish(mqtt_topic.encode(), mqtt_payload_str.encode() )

        mqttc.disconnect()

        # TODO...
        # if resp.status_code != INFLUXDB_WRITE_SUCCESS_CODE:
        #     msg_str = f"{get_current_datetime_str()}: {str(e)}; "
        #     msg_str += "Error while sending measurement in post_measurement(); "
        #     msg_str += f"Returned InfluxDB status_code is {str(resp.status_code)}; "
        #     msg_str += f"Returned InfluxDB reason is {resp.reason.decode('utf-8')};"
        #     msg_str += f"The raw response is {resp.text}\n\n"
        #     write_log(msg_str, print_msg=True)
    except Exception as e:
        msg_str = f"{get_current_datetime_str()}: {str(e)}; An unknown error occurred while sending measurement in post_measurement();"
        write_log(msg_str, print_msg=True)
        if STOP_ON_EXCEPTION:
            raise